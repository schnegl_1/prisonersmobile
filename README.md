# Prisoners
### Mobile Games Project WS 2019/20

Project Member: Martin Schneglberger

#### Changes since presentation

- Added 'Cancel' button when being in 'Upgrade' mode to abort upgrading
- Made game easier/slower
- Created flashing indicator where to place canons
- Local/Device Highscore
- Turret price now increases with every turret placed
- Levels increase to linearly anymore, user has to surive longer to level up with each level
- Prisoners caught by a canon add less money
- Smaller bugfixes (turret outside of prison, to money substracted in some scenarios)

### Project Hierarchy

- Assets
	- Animators: Contains the Animator for the prisoners
	- External: Contains all assets I got from the Asset Store (Wire Fences, Rocks, Trees, Towers, Prisoners...)
	- Materials: Contains self-defined materials
	- Scenes: Contains the (main) PrisonersScene
	- Scripts: Contains all self-defined scripts (see below)
	- Sound: Contains the sounds used for this game

- Scripts:
	- UI
		- `ColorLerp`: Used for the fading color of the plane indicating valid turret positions
		- `DrawCircle`: Used to draw the range-circle around the turrets
		- `RotateLight`: The red floodlight searching for the prisoners
	- `GameManager`:
	    - Manages the whole state(s) of the game (current level, button presses, tell canon- /prisonerspawner to spawn game objects, Game Over, (Re)start game...
	- `CanonSpawner`:
	    - After the `GameManager` forwarded the UIAction to place a canon, this spawner listens for the next mouse click and places the turret if the position is valid
	- `Prisoner`:
	    - Controls the behavior of a prisoner (speed, ...) as well as the actions when being caught, escaping etc. 
	- `PrisonerSpawner`:
	    - Spawns a certain amount of prisoners at a fixed time interval
	- `PrisonerSMBehaviour`:
	    - StateMachineBehaviour used to animate the prisoners
	- `ShootSound`:
	    - Plays the 'shoot' sound whenever a click/touch is done
	- `Wall`:
	    - Just used to detect if a prisoner bumped into the wall this script is attached to