﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour, IUpdateGameManagerTarget
{

    public const int CAUGHT_USER_POINTS = 20;
    public const int CAUGHT_CANON_POINTS = 5;
    public const int ESCAPTED_PUNISHMENT = 10;
    public int TURRET_PRICE = 40;
    public const int START_LIVES = 3;
    private const string HIGHSCORE_KEY = "highscore";

    public Text scoreText;
    public Text livesText;
    public Text levelText;
    public Text highScoreText;

    public PrisonersSpawner prisonersSpawner;
    public CanonSpawner canonSpawner;

    private int score = 0;
    private int lives = START_LIVES;
    private int level = 1;

    public GameObject startGameCanvas;
    public GameObject gameOverCanvas;

    private bool gameRunning;

    public Button canonButton;

    public GameObject upgradeContainer;
    public Button upgradeTurrentBtn;
    public Button cancelUpgradeBtn;

    private int canonsPlaced = 0;

    private MachineGun gunToUpgrade;
    private int upgradePrice = int.MaxValue;

    // Start is called before the first frame update
    void Start()
    {
        gameOverCanvas.SetActive(false);
        startGameCanvas.SetActive(true);
        gameRunning = false;
        prisonersSpawner.spawnPrisoners = false;

        canonButton.GetComponentInChildren<Text>().text = "Turret\n" + TURRET_PRICE.ToString();
        canonButton.GetComponent<Image>().color = Color.white;
        canonButton.interactable = false;

        upgradeContainer.SetActive(false);
        upgradeTurrentBtn.interactable = false;

        livesText.text = "Lives: " + lives.ToString();
        levelText.text = "Level: " + level.ToString();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void PrisonerEscaped()
    {
        if (!gameRunning) return;

        if (score - ESCAPTED_PUNISHMENT < 0) score = 0;
        else score -= ESCAPTED_PUNISHMENT;
        lives--;
        livesText.text = "Lives: " + lives.ToString();
        UpdateScore();
        if (lives <= 0)
        {
            GameOver();
        }
    }

    public void PrisonerCaughtByUser()
    {
        score += CAUGHT_USER_POINTS;
        UpdateScore();
    }

    public void PrisonerCaughtByCanon()
    {
        if (gameRunning)
        {
            score += CAUGHT_CANON_POINTS;
            UpdateScore();
        }
    }

    public void PlaceCanonBtnClicked()
    {
        canonSpawner.AllowPlacingOfCanon();
        canonButton.GetComponent<Image>().color = Color.red;
    }

    public void CanonPlaced()
    {
        canonButton.GetComponent<Image>().color = Color.white;
        canonButton.interactable = false;
        score -= TURRET_PRICE;
        if (score < 0) score = 0;
        canonsPlaced++;
        UpdateScore();
        TURRET_PRICE = TURRET_PRICE * 3;
    }

    private void UpdateScore()
    {
        scoreText.text = "Money: " + score.ToString();
        canonButton.GetComponentInChildren<Text>().text = "Turret\n" + (TURRET_PRICE).ToString();

        canonButton.interactable = score >= TURRET_PRICE;
        upgradeTurrentBtn.interactable = score >= upgradePrice;
    }

    private void GameOver()
    {
        prisonersSpawner.spawnPrisoners = false;
        gameRunning = false;
        highScoreText.text = "Highscore: Level " + PlayerPrefs.GetInt(HIGHSCORE_KEY, 1).ToString();
        gameOverCanvas.SetActive(true);
    }

    public void StartGame()
    {
        startGameCanvas.SetActive(false);
        RestartGame();
    }

    public void RestartGame()
    {
        prisonersSpawner.DestroyPrisoners();
        canonSpawner.DestroyCanons();
        canonSpawner.BlockPlacingOfCanon();

        lives = START_LIVES;
        livesText.text = "Lives: " + lives.ToString();
        score = 0;
        level = 1;
        levelText.text = "Level: " + level.ToString();

        gameOverCanvas.SetActive(false);
        canonsPlaced = 0;

        upgradeTurrentBtn.interactable = false;
        upgradeContainer.SetActive(false);
        canonButton.GetComponent<Image>().color = Color.white;
        canonButton.interactable = false;
        canonButton.gameObject.SetActive(true);

        UpdateScore();
        gameRunning = true;
        prisonersSpawner.spawnPrisoners = true;
    }

    public void SetLevel(int currentLevel)
    {
        level = currentLevel;
        levelText.text = "Level: " + level.ToString();

        if (level > PlayerPrefs.GetInt(HIGHSCORE_KEY, 0))
        {
            PlayerPrefs.SetInt(HIGHSCORE_KEY, level);
        }
    }

    public void ShowUpdateButton(int price, MachineGun gun)
    {
        gunToUpgrade = gun;
        upgradePrice = price;

        canonButton.gameObject.SetActive(false);

        upgradeTurrentBtn.interactable = price <= score;
        upgradeContainer.SetActive(true);
        upgradeTurrentBtn.GetComponentInChildren<Text>().text = ("Upgrade\n" + price).ToString();
    }

    public void CancelUpgrade()
    {
        gunToUpgrade = null;
        upgradePrice = int.MaxValue;

        upgradeContainer.SetActive(false);
        canonButton.gameObject.SetActive(true);
    }

    public void UpgradeCanon()
    {
        gunToUpgrade.Upgrade();
        canonButton.gameObject.SetActive(true);
        this.score -= upgradePrice;

        UpdateScore();

        upgradeTurrentBtn.interactable = false;
        upgradeContainer.SetActive(false);
    }

}
