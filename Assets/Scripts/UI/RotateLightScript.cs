﻿using System.Collections;
using UnityEngine;

public class RotateLightScript : MonoBehaviour
{
    public float rotationSpeed;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TurnLight(rotationSpeed));
    }

    private IEnumerator TurnLight(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            transform.Rotate(0, 10, 0);
        }
    }
}
