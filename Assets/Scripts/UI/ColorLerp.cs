﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorLerp : MonoBehaviour
{
    public Color[] colors;
    Color color;
    float t = 0;
    float i = 0.025f;
    bool change = false;

    void Awake()
    {
        color = colors[1];
    }

    void Update()
    {
        GetComponent<Renderer>().material.color = Color.Lerp(colors[1], colors[0], t);
        if (!change)
            t += i;
        else
            t -= i;
        if (t >= 1)
            change = true;
        if (t <= 0)
            change = false;
    }
}
