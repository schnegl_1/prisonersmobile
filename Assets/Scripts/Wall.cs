﻿using UnityEngine;

public class Wall : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!(other is CapsuleCollider)) return;

        var prisoner = other.gameObject.GetComponent<Prisoner>();

        if (prisoner != null)
        {
            prisoner.Escape();
        }
    }
}
