﻿using System.Collections;
using UnityEngine;

public class MachineGun : MonoBehaviour
{
    public const int MAX_LEVEL = 2;

    public GameManager gameManager;

    private Animator animator;

    private float maxRange = 4f;
    private bool isCoroutineExecuting = false;
    private Transform gunBarrel;
    private GameObject currentTarget;

    private AudioSource audioData;
    private ParticleSystem particles;

    private int currentLevel = 1;


    void Start()
    {
        audioData = GetComponent<AudioSource>();
        gunBarrel = GetComponentInChildren<Transform>().Find("acs17_part1_Gun0").GetComponentInChildren<Transform>().Find("head");
        particles = gunBarrel.GetComponent<ParticleSystem>();
        animator = GetComponentInChildren<Animator>();
        particles.Stop();
        InvokeRepeating("FindTarget", 0, 0.5f);
    }

    void Update()
    {
        if (currentTarget != null)
        {
            gunBarrel.LookAt(currentTarget.transform);
            gunBarrel.Rotate(-90, 90, 180);
        }
    }

    private void FindTarget()
    {
        GameObject[] prisoners = GameObject.FindGameObjectsWithTag("PRISONER");
        float minDistance = Mathf.Infinity;

        foreach (GameObject prisoner in prisoners)
        {
            if ((prisoner.GetComponent(typeof(Prisoner)) as Prisoner).IsAlive())
            {
                float distance = Vector3.Distance(this.gunBarrel.transform.position, prisoner.transform.position);

                if (distance < minDistance && distance < (maxRange * System.Math.Pow(1.5d, currentLevel - 1)))
                {
                    currentTarget = prisoner;
                    minDistance = distance;
                }
            }
        }
        if (currentTarget != null)
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Fire", true);
            audioData.Play(0);
            particles.Play();
            StartCoroutine(EliminateAfterTime(0.5f));
        }
    }

    public void Upgrade()
    {
        if (currentLevel < MAX_LEVEL)
        {
            currentLevel++;
            gameObject.GetComponent<DrawCircleAroundObj>().radius *= (float)System.Math.Pow(1.5d, currentLevel - 1);
            particles.startSpeed *= (float)System.Math.Pow(1.5d, currentLevel - 1);
            gameObject.GetComponent<DrawCircleAroundObj>().DoRenderer();
        }
    }

    void OnMouseDown()
    {
        if (currentLevel < MAX_LEVEL)
        {
            gameManager.ShowUpdateButton(currentLevel * 200, this);
        }
    }

    IEnumerator EliminateAfterTime(float time)
    {
        if (isCoroutineExecuting)
            yield break;
        isCoroutineExecuting = true;
        yield return new WaitForSeconds(time);
        (currentTarget.GetComponent(typeof(Prisoner)) as Prisoner).Shoot();
        currentTarget = null;
        particles.Clear();
        particles.Stop();
        animator.SetBool("Fire", false);
        animator.SetBool("Idle", true);
        audioData.Stop();
        isCoroutineExecuting = false;
    }
}
