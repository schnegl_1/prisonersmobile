﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Prisoner : MonoBehaviour
{

    private const int DESTROY_AFTER_TIME = 2;

    public GameObject gameManager;
    private float speed = 0.3f;
    private Animator animator;
    private bool isCaught = false;
    private bool isEscaped = false;

    // Use this for initialization
    void Start()
    {
        speed = Random.Range(0.6f, 1.2f);
        animator = GetComponent<Animator>();

        // Find a reference to the ExampleStateMachineBehaviour
        PrisonerSMBehaviour prisonerSMB = animator.GetBehaviour<PrisonerSMBehaviour>();
        prisonerSMB.prisoner = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isCaught && !isEscaped) transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    void Catch()
    {
        isCaught = true;
        animator.SetBool("isCaught", true);
        ExecuteEvents.Execute<IUpdateGameManagerTarget>(gameManager, null, (x, y) => x.PrisonerCaughtByUser());
        Destroy(gameObject, DESTROY_AFTER_TIME);
    }

    public void Shoot()
    {
        isCaught = true;
        animator.SetBool("isCaught", true);
        ExecuteEvents.Execute<IUpdateGameManagerTarget>(gameManager, null, (x, y) => x.PrisonerCaughtByCanon());
        Destroy(gameObject, DESTROY_AFTER_TIME);
    }

    public void Escape()
    {
        if (IsAlive())
        {
            Handheld.Vibrate();
            isEscaped = true;
            animator.SetBool("isEscaped", true);
            ExecuteEvents.Execute<IUpdateGameManagerTarget>(gameManager, null, (x, y) => x.PrisonerEscaped());
        }
    }

    public bool IsAlive()
    {
        return !isCaught && !isEscaped;
    }

    private void OnMouseDown()
    {
        if (!isCaught) Catch();
    }


    public void escapedAnimationEnded()
    {
        Destroy(gameObject);
    }

}