﻿using UnityEngine;

public class ShootSound : MonoBehaviour
{
    public AudioSource sound;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnGUI()
    {
        if (Input.GetMouseButtonDown(0))
        {
            sound.Play();
        }
    }
}
