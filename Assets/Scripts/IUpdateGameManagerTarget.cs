﻿using UnityEngine.EventSystems;

public interface IUpdateGameManagerTarget : IEventSystemHandler
{
    void PrisonerCaughtByUser();
    void PrisonerCaughtByCanon();
    void PrisonerEscaped();
    void CanonPlaced();
    void SetLevel(int level);
}
