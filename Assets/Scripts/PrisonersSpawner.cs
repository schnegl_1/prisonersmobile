﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class PrisonersSpawner : MonoBehaviour
{

    public int NEXT_LEVEL_AFTER = 5;

    public GameObject[] prisoners;
    public GameObject gameManager;
    public float spawnTime = 1.4f;
    private int spawned = 0;

    public bool spawnPrisoners = false;

    private ArrayList spawnedPrisoners = new ArrayList();

    private int currentLevel = 1;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Spawn()
    {
        if (!spawnPrisoners)
        {
            return;
        }
        GameObject prisoner = prisoners[0];

        int newLevel = spawnedPrisoners.Count / NEXT_LEVEL_AFTER;
        if (newLevel > currentLevel)
        {
            currentLevel = newLevel;
            Debug.Log("Level up" + currentLevel);
            NEXT_LEVEL_AFTER = (int)(NEXT_LEVEL_AFTER * 1.5);
            ExecuteEvents.Execute<IUpdateGameManagerTarget>(gameManager, null, (x, y) => x.SetLevel(currentLevel));
        }

        int prisonersToSpawn = Random.Range(1, currentLevel);

        for (int i = 0; i < prisonersToSpawn; i++)
        {
            // Create an instance of the prisoner prefab at the spawn point's position and with a random rotation.
            Vector3 rotation = new Vector3(0, Random.Range(0, 360), 0);
            GameObject newPrisoner = Instantiate(prisoner, this.transform.position, this.transform.rotation);
            newPrisoner.transform.Rotate(rotation);
            newPrisoner.AddComponent<Prisoner>();
            newPrisoner.AddComponent<Animation>();
            newPrisoner.tag = "PRISONER";
            (newPrisoner.GetComponent(typeof(Prisoner)) as Prisoner).gameManager = gameManager;
            spawned++;
            spawnedPrisoners.Add(newPrisoner);
        }
    }

    public void DestroyPrisoners()
    {
        foreach (GameObject prisoner in spawnedPrisoners)
        {
            Destroy(prisoner);
        }
        spawnedPrisoners = new ArrayList();
        currentLevel = 1;
        NEXT_LEVEL_AFTER = 5;
    }
}
