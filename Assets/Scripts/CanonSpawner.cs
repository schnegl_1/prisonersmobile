﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class CanonSpawner : MonoBehaviour
{
    public const float MIN_DISTANCE_TO_SPAWNER = 5f;

    public GameObject canonToPlace;
    public GameObject spawnerPreserve;
    public GameManager gameManager;

    private bool allowPlacingOfCanon = false;

    private ArrayList placedCanons = new ArrayList();

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (allowPlacingOfCanon && Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (!hit.transform.gameObject.CompareTag("GROUND")) return;

                float distance = Vector3.Distance(gameObject.transform.position, hit.point);
                if (distance >= MIN_DISTANCE_TO_SPAWNER)
                {
                    GameObject canon = Instantiate(canonToPlace, hit.point, spawnerPreserve.transform.rotation);
                    canon.AddComponent<MachineGun>();
                    canon.GetComponent<MachineGun>().gameManager = gameManager;
                    canon.transform.position = new Vector3(canon.transform.position.x, 0, canon.transform.position.z);
                    ExecuteEvents.Execute<IUpdateGameManagerTarget>(gameManager.gameObject, null, (x, y) => x.CanonPlaced());
                    allowPlacingOfCanon = false;
                    spawnerPreserve.SetActive(false);
                    placedCanons.Add(canon);
                }
            }

        }
    }

    public void BlockPlacingOfCanon()
    {
        allowPlacingOfCanon = false;
        spawnerPreserve.SetActive(false);
    }

    public void AllowPlacingOfCanon()
    {
        allowPlacingOfCanon = true;
        spawnerPreserve.SetActive(true);
    }

    public void DestroyCanons()
    {
        foreach (GameObject canon in placedCanons)
        {
            Destroy(canon);
        }
        placedCanons = new ArrayList();
    }
}

